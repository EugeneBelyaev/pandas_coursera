import pandas as pd
import numpy as np
from string import digits


# Energy supply
with pd.ExcelFile('Energy Indicators.xls') as xls:
    energy = pd.read_excel(xls, index_col=None, na_values=['NA'],skiprows=[i for i in range(17)], skip_footer=35 )

energy = energy.drop(['Unnamed: 0', 'Unnamed: 1'], axis=1)

energy.rename(columns={"Unnamed: 2": "Country", "Petajoules": "Energy Supply",
                    'Gigajoules': 'Energy Supply per Capita', '%': '% Renewable'}, inplace=True)

energy.dropna(inplace=True)

to_change = {}
for country in energy['Country']:
    if '(' in country:
        line = country.split('(')
        res = line[0].rstrip()
        to_change[country]=res
        continue
    remove_digits = str.maketrans('', '', digits)
    res = country.translate(remove_digits)
    to_change[country]=res

energy['Country'].update(energy['Country'].map(to_change))

energy.replace({'Country':{"Republic of Korea": "South Korea",
                "United States of America": "United States",
                "United Kingdom of Great Britain and Northern Ireland": "United Kingdom",
                "China, Hong Kong Special Administrative Region": "Hong Kong"}},
                inplace=True)
energy.replace('...', np.NaN, inplace=True)
energy['Energy Supply'] *=  1000000
energy.set_index('Country', inplace=True)


# World bank data

GDP = pd.read_csv('world_bank.csv', skiprows=4)

GDP.rename(columns={"Country Name": "Country"}, inplace=True)

col_to_change = {"Korea, Rep.": "South Korea", 
"Iran, Islamic Rep.": "Iran",
"Hong Kong SAR, China": "Hong Kong"}

GDP['Country'].update(GDP['Country'].map(col_to_change))
GDP.set_index('Country', inplace=True)


# Sciamgo Journal and Country Rank data for Energy Engineering and Power Technology
with pd.ExcelFile('scimagojr-3.xlsx') as xlsx:
    ScimEn = pd.read_excel(xlsx, index_col=None, na_values=['NA']).set_index('Country')


# Merging
def answer_one():
    col_to_keep = ['Rank', 'Documents', 'Citable documents', 'Citations', 'Self-citations',
                'Citations per document','H index', 'Energy Supply', 'Energy Supply per Capita',
                '% Renewable', '2006', '2007', '2008', '2009', '2010', '2011','2012', '2013', '2014', '2015']

    merged = pd.merge(energy, GDP, how='right', left_index=True, right_index=True)
    merged2 = pd.merge(merged, ScimEn, how='right', left_index=True, right_index=True)
    result = merged2[col_to_keep]
    
    return result[:15]


s = answer_one()

def answer_two():
    union = energy.index | GDP.index | ScimEn.index
    inter = energy.index & GDP.index & ScimEn.index
    
    return len(union) - len(inter)


def answer_three():
    Top15 = answer_one()[['2006', '2007', '2008', '2009', '2010', '2011','2012', '2013', '2014', '2015']].mean(axis=1).sort_values(ascending=False)
    
    return pd.Series(Top15, name='avgGDP')

Top15 = answer_one()[['Self-citations', 'Citations']]





def answer_four():
    gdp = answer_one().loc[['United Kingdom']]
    return gdp.at['United Kingdom' ,'2015'] - gdp.at['United Kingdom' ,'2006']



def answer_five():
    
    return answer_one()['Energy Supply per Capita'].mean()
    
def answer_six():
    
    Top15 = answer_one()['% Renewable']
    top = Top15.idxmax()
    return (top, Top15.at[top])

def answer_seven():
    Top15 = answer_one()
    Top15['ratio'] = [row['Self-citations']/ row['Citations'] for index, row in Top15.iterrows()]

    return (Top15['ratio'].idxmax(), Top15['ratio'].max())



def answer_eight():

    Top15 = answer_one()
    Top15['population'] = [row['Energy Supply']/ row['Energy Supply per Capita'] for index, row in Top15.iterrows()]
    TopPop = Top15['population'].sort_values(ascending=False)
    return (TopPop.index[TopPop==TopPop[2]]).format()[0]



def answer_nine():

    Top15 = answer_one()
    Top15['population'] = [row['Energy Supply']/ row['Energy Supply per Capita'] for index, row in Top15.iterrows()]
    Top15['citable documents per person'] = [row['Citable documents']/ row['population'] for index, row in Top15.iterrows()]
    corr = Top15['citable documents per person'].corr(Top15['Energy Supply per Capita'])
    return corr



def answer_ten():
    
    Top15 = answer_one()
    med = Top15['% Renewable'].median()
    Top15['HighRenew'] = Top15['% Renewable']>=med
    Top15['HighRenew'] = Top15['HighRenew'].apply(lambda x:1 if x else 0)
    Top15.sort_values(by='Rank', inplace=True)
    return Top15['HighRenew']




ContinentDict  = {'China':'Asia', 
                  'United States':'North America', 
                  'Japan':'Asia', 
                  'United Kingdom':'Europe', 
                  'Russian Federation':'Europe', 
                  'Canada':'North America', 
                  'Germany':'Europe', 
                  'India':'Asia',
                  'France':'Europe', 
                  'South Korea':'Asia', 
                  'Italy':'Europe', 
                  'Spain':'Europe', 
                  'Iran':'Asia',
                  'Australia':'Australia', 
                  'Brazil':'South America'}




def answer_eleven():
    
    Top15 = answer_one()
    groups = pd.DataFrame(columns = ['size', 'sum', 'mean', 'std'])
    Top15['Estimate Population'] = Top15['Energy Supply'] / Top15['Energy Supply per Capita']
    for group, frame in Top15.groupby(ContinentDict):
        groups.loc[group] = [len(frame), frame['Estimate Population'].sum(), frame['Estimate Population'].mean(), frame['Estimate Population'].std()]
    return groups



def answer_twelve():
    
    Top15 = answer_one().reset_index()
    Top15['Continent'] = [ContinentDict[country] for country in Top15['Country']]
    Top15['bins'] = pd.cut(Top15['% Renewable'],5)
    return Top15.groupby(['Continent','bins']).size()

def answer_thirteen():
    
    Top15 = answer_one()
    Top15['PopEst'] = (Top15['Energy Supply'] / Top15['Energy Supply per Capita']).astype(float)
    return Top15['PopEst'].apply(lambda x: '{0:,}'.format(x))




